type EstimatedTax = {
    tax: number
}

export const estimateTax = async (price: number, quantity: number, key: string): Promise<number> => {
    const basePrice = await fetch("https://lajtkep.dev/steam/priceRequest.php?market_hash_name=" + encodeURIComponent(key), { next: { revalidate: 300 } })
        .then(response => response.json())
        .then(({ lowest_price }: { lowest_price: string }) => Number(lowest_price.replaceAll(/,/g, ".").replace("€", "")));

    const rate = await fetch(`https://data.kurzy.cz/json/meny/b[2].json`, { next: { revalidate: 4 * 60 * 60 * 1000 }})
        .then(response => response.json())
        .then(response => Number(response.kurzy.EUR.dev_stred));

    const vat = basePrice * 0.85;
    const profit = (vat * 100 - price) * quantity;
    const tax = profit * 0.21;

    return Math.ceil(tax * rate / 100);
}