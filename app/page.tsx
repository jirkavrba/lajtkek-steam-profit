import { estimateTax } from "@/lib/1fp201";
import Image from "next/image";
import Link from "next/link";

export default async function Home() {
  "use server";

  const prices = {
    cs20: 7,
    prisma2: 5,
    dangerZone: 6
  }

  const quantities = {
    cs20: 5000,
    prisma2: 4000,
    dangerZone: 1975
  }

  const estimates = await Promise.all([
    estimateTax(prices.cs20, quantities.cs20, "CS20 Case"),
    estimateTax(prices.prisma2, quantities.prisma2, "Prisma 2 Case"),
    estimateTax(prices.dangerZone, quantities.dangerZone, "Danger Zone Case")
  ]);

  const [cs20, prisma2, dangerZone] = estimates;
  const total = estimates.reduce((sum, current) => sum + current);

  return (
    <main className="flex flex-col items-center justify-center min-w-screen min-h-screen">
      <h1 className="text-center text-2xl px-5 pt-5 md:pt-0 md:text-4xl font-bold">Kolik Matěj dluží finančáku až prodá bedničky?</h1>

      <div className="flex flex-col md:flex-row items-center justify-center gap-2 md:gap-10 mt-10">
        <div className="flex flex-col items-center">
          <Image src={"/csgo20.png"} width={128} height={128} alt="CSGO 20 Case" />
          <div className="text-xl md:text-3xl font-bold">{cs20},- Kč</div>
        </div>
        <div className="text-3xl text-neutral-400">+</div>
        <div className="flex flex-col items-center">
          <Image src={"/prisma-2.png"} width={128} height={128} alt="Prisma 2 Case" />
          <div className="text-xl md:text-3xl font-bold">{prisma2},- Kč</div>
        </div>
        <div className="text-3xl text-neutral-400">+</div>
        <div className="flex flex-col items-center">
          <Image src={"/danger-zone.png"} width={128} height={128} alt="Danger Zone Case" />
          <div className="text-xl md:text-3xl font-bold">{dangerZone},- Kč</div>
        </div>
        <div className="text-3xl text-neutral-400">=</div>
        <div className="text-2xl md:text-5xl font-bold bg-gradient-to-r from-yellow-600 to-red-400 bg-clip-text text-transparent">{total},- Kč</div>
      </div>

      <Link href="/source.gif" className="mt-20 underline cursor-pointer">Source</Link>
    </main>
  )
}
